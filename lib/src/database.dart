import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

const String DB_NAME = "todo_db.db";
const String TABLE_NAME = "todo_list";

Future<dynamic> get database async {
  return await _initDatabase();
}

_initDatabase() async {
  return await openDatabase(join(await getDatabasesPath(), DB_NAME),
      onCreate: (db, version) {
    return db.execute(
      'CREATE TABLE $TABLE_NAME(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title TEXT, content TEXT, has_done INTEGER)',
    );
  }, version: 1);
}
