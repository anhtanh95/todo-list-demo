import 'package:flutter/material.dart';
import 'package:todo_list_demo/src/blocs/todo_list_bloc.dart';
import 'package:todo_list_demo/src/models/todo_item.dart';
import 'package:todo_list_demo/src/widgets/card_item.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo List Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'TODO List Demo'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late List<TodoItem> _list;
  TodoListBloc? _todoListBloc;
  final titleTextFieldController = TextEditingController();
  final contentTextFieldController = TextEditingController();


  @override
  void dispose() {
    super.dispose();
    _todoListBloc?.dispose();
    titleTextFieldController.dispose();
    contentTextFieldController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: StreamBuilder(
        stream: _todoListBloc?.allTodoList,
        builder: (context, AsyncSnapshot<List<TodoItem>> snapshot) {
          if (snapshot.hasData || snapshot.data != null) {
            List<TodoItem> listItem = snapshot.data!;
            if (listItem.isNotEmpty) {
              _list = listItem;
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView.builder(
                  //key: _listKey,
                  itemCount: _list.length,
                  itemBuilder: _buildItem,
                ),
              );
            } else {
              return const Text(
                  "There is no data. Press (+) to insert new TODO");
            }
          } else {
            return const Text("There is no data. Press (+) to insert new TODO");
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: _buildDialogCreateTodo,
          );
        },
        tooltip: 'Add new TODO',
        child: const Icon(Icons
            .add), // This trailing comma makes auto-formatting nicer for build methods.
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  @override
  void initState() {
    super.initState();
    _todoListBloc = TodoListBloc();
    _todoListBloc?.fetchAllTodoList();
  }

  Widget _buildItem(BuildContext context, int index) {
    return CardItem(
      item: _list[index],
      selected: _list[index].hasDone,
      onTap: () {
        showDialog(
          context: context,
          builder: (context) {
            return _buildDialogShowTodo(context, _list[index]);
          }
        );
      },
      onRemove: () {
        _todoListBloc?.deleteTodoItem(_list[index]);
      },
      onMark: (bool? newValue) {
        _todoListBloc?.updateTodoItemMark(_list[index], newValue ?? false);
      },
    );
  }

  Widget _buildDialogCreateTodo(BuildContext context) {
    titleTextFieldController.text = "";
    contentTextFieldController.text = "";
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      elevation: 16,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(height: 20),
          Center(child: Text('New TODO Item', style: Theme
              .of(context)
              .textTheme
              .headline5)),
          const SizedBox(height: 20),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text('Title'),
          ),
          const SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextField(
              controller: titleTextFieldController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter title for your TODO',
              ),
            ),
          ),
          const SizedBox(height: 20),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text('Your TODO content'),
          ),
          const SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextField(
              maxLines: 3,
              controller: contentTextFieldController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter title for your TODO',
                fillColor: Colors.black12,
                filled: true,
              ),
            ),
          ),
          Center(
            child: TextButton(
                onPressed: () {
                  _todoListBloc?.insertTodoItem(TodoItem(
                      title: titleTextFieldController.text.isNotEmpty
                          ? titleTextFieldController.text
                          : "Default TODO",
                      content: contentTextFieldController.text,
                      hasDone: false));
                  Navigator.pop(context);
                },
                child: const Text("Create")
            ),
          )
        ],
      ),
    );
  }


  Widget _buildDialogShowTodo(BuildContext context, TodoItem item) {
    titleTextFieldController.text = item.title;
    contentTextFieldController.text = item.content;
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      elevation: 16,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(height: 20),
          Center(child: Text('Edit TODO Item', style: Theme
              .of(context)
              .textTheme
              .headline5)),
          const SizedBox(height: 20),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text('Title'),
          ),
          const SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextField(
              controller: titleTextFieldController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter title for your TODO',
              ),
            ),
          ),
          const SizedBox(height: 20),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text('Your TODO content'),
          ),
          const SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextField(
              maxLines: 3,
              controller: contentTextFieldController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter title for your TODO',
                fillColor: Colors.black12,
                filled: true,
              ),
            ),
          ),
          Center(
            child: TextButton(
                onPressed: () {
                  var title = titleTextFieldController.text.isNotEmpty
                      ? titleTextFieldController.text
                      : "Default TODO";
                  var content = contentTextFieldController.text;
                  _todoListBloc?.updateTodoItem(item, title, content);
                  Navigator.pop(context);
                },
                child: const Text("Save")
            ),
          )
        ],
      ),
    );
  }
}
