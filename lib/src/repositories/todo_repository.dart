import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_list_demo/src/database.dart';
import 'package:todo_list_demo/src/models/todo_item.dart';

class TodoRepository {

  Future<List<TodoItem>> fetchTodoList() async {
    // Web version cannot use sqlflite, so I will use mock data instead
    if (!kIsWeb) {
      final db = await database;
      final List<Map<String, dynamic>> maps = await db.query(TABLE_NAME);
      return List.generate(maps.length, (i) {
        return TodoItem(
            id: maps[i]['id'],
            title: maps[i]['title'],
            content: maps[i]['content'],
            hasDone: maps[i]['has_done'] == 1);
      });
    } else {
      List<TodoItem> listTodo = <TodoItem>[];
      listTodo.add(TodoItem(id: 1, title: "TODO Item 1", content: "Do the test!", hasDone: false));
      listTodo.add(TodoItem(id: 2, title: "TODO Item 2", content: "Play the game!", hasDone: false));
      listTodo.add(TodoItem(id: 3, title: "TODO Item 3", content: "Go camping!", hasDone: true));
      listTodo.add(TodoItem(id: 4, title: "TODO Item 4", content: "Play music", hasDone: false));
      listTodo.add(TodoItem(id: 5, title: "TODO Item 5", content: "Go picnic", hasDone: false));
      return Future.value(listTodo);
    }
  }

  Future<void> insertTodo(TodoItem todoItem) async {
    if (!kIsWeb) {
      final db = await database;
      final insertedId = await db.insert(
        TABLE_NAME,
        todoItem.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      todoItem.id = insertedId;
    }
  }

  Future<int> deleteTodo(int id) async {
    if (!kIsWeb) {
      final db = await database;
      return await db.delete(
        TABLE_NAME,
        where: 'id = ?',
        whereArgs: [id],
      );
    } else {
      return 1;
    }
  }

  Future<void> updateTodo(TodoItem todoItem) async {
    if (!kIsWeb) {
      final db = await database;
      await db.update(
        TABLE_NAME,
        todoItem.toMap(),
        where: 'id = ?',
        whereArgs: [todoItem.id],
      );
    }
  }
}