class TodoItem {
  int id;
  String title;
  String content;
  bool hasDone;

  TodoItem(
      {this.id = -1,
      required this.title,
      required this.content,
      required this.hasDone});

  Map<String, dynamic> toMap() {
    return {
      //'id': id,
      'title': title,
      'content': content,
      'has_done': hasDone ? 1 : 0
    };
  }

  @override
  String toString() {
    return 'TodoItem{id: $id, title: $title, content: $content, hasDone: $hasDone}';
  }
}
