import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:todo_list_demo/src/models/todo_item.dart';

class CardItem extends StatelessWidget {
  const CardItem({
    Key? key,
    this.onTap,
    this.onRemove,
    this.onMark,
    this.selected = false,
    required this.item,
  })  : super(key: key);

  final VoidCallback? onTap;
  final VoidCallback? onRemove;
  final ValueChanged<bool?>? onMark;
  final TodoItem item;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.headline5!;
    if (selected) {
      textStyle = textStyle.copyWith(color: Colors.lightGreenAccent[400]);
    }
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: onTap,
        child: SizedBox(
          height: 80.0,
          child: Card(
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      if (kIsWeb)
                        Text(item.title, style: textStyle)
                      else
                        Text('[ID:${item.id}] ${item.title}', style: textStyle),
                      Text(
                        item.content,
                        style: Theme.of(context).textTheme.headline6,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Expanded(flex: 1, child: Text("Remove")),
                      Expanded(
                        flex: 3,
                        child: IconButton(
                          icon: const Icon(Icons.remove_circle),
                          onPressed: onRemove,
                          tooltip: 'remove TODO',
                        )
                      ),
                    ],
                  ),
                  const SizedBox(width: 5),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Expanded(flex: 1, child: Text("Done")),
                      Expanded(
                        flex: 3,
                        child: Checkbox(
                            value: selected,
                            onChanged: onMark),
                      ),
                    ],
                  ),
                ],
              ),
          ),
        ),
      ),
    );
  }
}