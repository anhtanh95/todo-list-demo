import 'package:rxdart/rxdart.dart';
import 'package:todo_list_demo/src/models/todo_item.dart';
import 'package:todo_list_demo/src/repositories/todo_repository.dart';

class TodoListBloc {
  TodoRepository? _todoRepository;

  final _todoListFetcher = PublishSubject<List<TodoItem>>();
  Stream<List<TodoItem>> get allTodoList => _todoListFetcher.stream;

  List<TodoItem>? todoList;

  TodoListBloc() {
    _todoRepository = TodoRepository();
  }

  dispose() {
    _todoListFetcher.close();
  }

  fetchAllTodoList() async {
    todoList = await _todoRepository?.fetchTodoList();
    if (todoList != null) {
      _todoListFetcher.sink.add(todoList!);
    }
  }

  insertTodoItem(TodoItem item) async {
    await _todoRepository?.insertTodo(item);
    if (todoList != null) {
      todoList?.add(item);
      _todoListFetcher.sink.add(todoList!);
    }
  }

  deleteTodoItem(TodoItem item) async {
    int? affectedRows = await _todoRepository?.deleteTodo(item.id);
    if (todoList != null && affectedRows != null && affectedRows > 0) {
      _todoListFetcher.sink.add(todoList!);
      todoList?.remove(item);
    }
  }

  updateTodoItemMark(TodoItem item, bool markValue) async {
    if (todoList != null) {
      var newItem = todoList![todoList!.indexOf(item)];
      newItem.hasDone = markValue;
      await _todoRepository?.updateTodo(newItem);
      _todoListFetcher.sink.add(todoList!);
    }
  }

  updateTodoItem(TodoItem item, String title, String content) async {
    if (todoList != null) {
      var newItem = todoList![todoList!.indexOf(item)];
      newItem.title = title;
      newItem.content = content;
      await _todoRepository?.updateTodo(newItem);
      _todoListFetcher.sink.add(todoList!);
    }
  }
}